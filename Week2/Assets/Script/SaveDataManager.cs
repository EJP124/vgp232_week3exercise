using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class SaveDataManager
{
    public static string directory = "/SaveData/";
    public static string filename = "SaveData.txt";

    public static void Save(GameData gd)
    {
        string dir = Application.persistentDataPath + directory;

        if(!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
        string json = JsonUtility.ToJson(gd);
        File.WriteAllText(dir + filename, json);

        Debug.Log("Save in" + dir);
    }

    public static GameData Load()
    {
        string fullPath = Application.persistentDataPath + directory + filename;
        GameData gd = new GameData();

        if(File.Exists(fullPath))
        {
            string json = File.ReadAllText(fullPath);
            gd = JsonUtility.FromJson<GameData>(json);
        }
        else
        {
            Debug.Log("Save does not exist");
        }
        return gd;
    }
}
