using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveDataBinaryManager
{
    public static string directory = "/SaveData/";
    public static string filename = "SaveData.txt";

    public static void Save(GameData gd)
    {
        string dir = Application.persistentDataPath + directory;

        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(dir + filename);
        bf.Serialize(file, gd);
        file.Close();

        Debug.Log("Save in" + dir);
    }

    public static GameData Load()
    {
        string fullPath = Application.persistentDataPath + directory + filename;
        GameData gd = new GameData();

        if (File.Exists(fullPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(fullPath, FileMode.Open);
            gd = (GameData)bf.Deserialize(file);
            return gd;
        }
        else
        {
            Debug.Log("Save does not exist");
        }
        return gd;
    }
}

