using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Weapons 
{
    public string weaponName;
    public int damage;
    public int weight;
}
