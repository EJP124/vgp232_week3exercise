using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Weapons> weapons = new List<Weapons>();
    public int itemSlots;
    public List<Meds> meds = new List<Meds>();

    private GameObject bagReference;
}
