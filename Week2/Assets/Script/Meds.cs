using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum medsType { heal, pain, Fracture };

[System.Serializable]
public class Meds
{
    public string medsName;
    public int healIndex;
    [SerializeField] private medsType medsItem;
}
