using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class GameData
{
    public string playerNmae;
    public int currentLevel;
    public int savePointId;

    public Weapons currentEquippedWeapon;
    public Meds currentEquippedMeds;

    public List<Item> inventory;
    public List<Skill> skills;
}
